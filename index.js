//Print message and options
const message = 'Choose an option:';
const option1 = '1. Read package.json';
const option2 = '2. Display OS info';
const option3 = '3. Start HTTP server';

console.log(message, '\n', option1, '\n', option2, '\n', option3);

//Ask for input and handle response 
const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
});

readline.question('Type a number: ', (number) => {
  if (number == 1) {
    printPackageJson();
  } else if (number == 2) {
    printOSInfo();
  } else if (number == 3) {
    startServer();
  } else {
    console.log('Invalid option.');
  }
  readline.close();
});

//option 1
const fs = require('fs');
const printPackageJson = () => {
  fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
    console.log(content);
  });
};

//option 2
const os = require('os');
const printOSInfo = () => {
  console.log('Getting os info');
  console.log(
    'SYSTEM MEMORY: ',
    (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + 'GB'
  );
  console.log(
    'FREE MEMORY: ',
    (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + 'GB'
  );
  console.log('CPU CORES: ' + os.cpus().length);
  console.log('ARCH: ' + os.arch());
  console.log('PLATFORM: ' + os.platform());
  console.log('RELEASE: ' + os.release());
  console.log('USER: ' + os.userInfo().username);
};

//option 3
const http = require('http');
const { PORT = 3000 } = process.env;
const startServer = () => {
  const server = http.createServer((req, res) => {
    if (req.url === '/') {
      res.write('Hello World!');
      res.end();
    }
  });
  console.log('Starting HTTP server...');
  server.listen(PORT);
  console.log(`listening on port ${PORT}...`);
};
